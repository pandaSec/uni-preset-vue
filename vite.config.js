import {defineConfig} from 'vite'
import uni from '@dcloudio/vite-plugin-uni'
// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        uni(),
    ],
    server: {
        proxy: {
            "/api": {
                // target: "http://localhost:8088",
                target: "http://www.wechao.xyz:8088",
                changeOrigin: true,
                rewrite: (path) => {
                    const newPath =  path.replace(/^\/api/, "");
                    // console.log(newPath, 'newPath')
                    return newPath
                }
            },
            "/mhxy": {
                // target: "http://localhost:8088",
                target: "http://www.wechao.xyz:8088",
                changeOrigin: true,
                // rewrite: (path) => {
                //     const newPath =  path.replace(/^\/api/, "");
                //     // console.log(newPath, 'newPath')
                //     return newPath
                // }
            },
        }
    }
})
