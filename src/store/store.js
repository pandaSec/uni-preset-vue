// 964,//空山新雨
//     965,//九天揽月
//     962,//水木清华
import axios from "axios";

const store = {
    rangeServerCode: [
        {value: "", text: "全部"},
        {value: "hn1q-jqhh", text: "河南1区&九曲黄河"},
        {value: "js1q-sdgt", text: "☆江苏1区-水调歌头"},
        {value: "lcmj-dzsf", text: "☆良辰美景-斗战胜佛"},
        {value: "txws-jlzy", text: "☆天下无双-九黎之影"},
        {value: "zj2q-jtly", text: "☆浙江2区-九天揽月"},
        {value: "bj3q-zjzd", text: "☆北京3区-紫禁之巅"},
        {value: "zj4q-csmy", text: "☆浙江4区-春色满园"},
        {value: "mysj-wlhx", text: "名扬三界-万里华夏"},
        {value: "tjq-jwm", text: "☆天津区-精武门"},
        {value: "sh1q-wft", text: "☆上海1区-晚芳亭"},
        {value: "zy-yssa", text: "追忆-一生所爱"},
        {value: "lcmj-ylyn", text: "良辰美景-一路有你"},
        {value: "lcmj-syg", text: "良辰美景-水云归"},
        // {value: "bj1q-srkl", text: "北京1区-生日快乐"},
        // {value: "sc1q-cdf", text: "四川1区-成都府"},
        // {value: "js1q-ksxy", text: "江苏1区-空山新雨"},
        // {value: "bj3q-smqh", text: "北京3区-水木清华"},
    ],
    rangeBaoshiCode: [
        {value: "", text: "全部"},
        {value: "黑宝石", stype: '4010', text: "黑宝石"},
        {value: "舍利子", stype: '4012', text: "舍利子"},
        {value: "太阳石", stype: '4002', text: "太阳石"},
        {value: "红玛瑙", stype: '4011', text: "红玛瑙"},
        {value: "月亮石", stype: '4003', text: "月亮石"},
        {value: "光芒石", stype: '4004', text: "光芒石"},
        {value: "翡翠石", stype: '4249', text: "翡翠石"},
        {value: "星辉石", stype: '4244', text: "星辉石"},
        {value: "金钱", text: "金钱"},
        {value: "精魄灵石", text: "精魄灵石"},
    ],
    nicknameRange: [
        {value: "", text: "全部"},
        {value: "九曲", text: "九曲"},
        {value: "水调", text: "水调"},
        {value: "斗战", text: "斗战"},
        {value: "九黎", text: "九黎"},
        {value: "九天", text: "九天"},
        {value: "万里", text: "万里"},
        {value: "紫禁", text: "紫禁"},
        // {value: "水木", text: "水木"},
        // {value: "dk", text: "dk"},
        // {value: "00100", text: "001"},
        // {value: "00200", text: "002"},
        // {value: "00300", text: "003"},
        // {value: "00400", text: "004"},
        // {value: "00500", text: "005"},
        // {value: "00600", text: "006"},
    ],
    mhbConf: [],
    async getMhbConf() {
        if (!store.mhbConf.length) {
            store.mhbConf = [
                {
                    "serverId": "hn1q-jqhh",
                    "minPrice": "675.60",
                    "createDate": "2023-11-12",
                    "serverName": "九曲黄河"
                },
                {
                    "serverId": "js1q-sdgt",
                    "minPrice": "398.90",
                    "createDate": "2023-11-12",
                    "serverName": "水调歌头"
                },
                {
                    "serverId": "zj4q-csmy",
                    "minPrice": "340.80",
                    "createDate": "2023-11-12",
                    "serverName": "春色满园"
                },
                {
                    "serverId": "txws-jlzy",
                    "minPrice": "278.10",
                    "createDate": "2023-11-12",
                    "serverName": "九黎之影"
                },
                {
                    "serverId": "bj3q-zjzd",
                    "minPrice": "534.50",
                    "createDate": "2023-11-12",
                    "serverName": "紫禁之巅"
                },
                {
                    "serverId": "zj2q-jtly",
                    "minPrice": "330.00",
                    "createDate": "2023-11-12",
                    "serverName": "九天揽月"
                },
                {
                    "serverId": "mysj-wlhx",
                    "minPrice": "597.90",
                    "createDate": "2023-11-12",
                    "serverName": "万里华夏"
                },
                {
                    "serverId": "bj3q-smqh",
                    "minPrice": "383.70",
                    "createDate": "2023-11-12",
                    "serverName": "水木清华"
                },
                {
                    "serverId": "zj1q-byf",
                    "minPrice": "468.90",
                    "createDate": "2023-11-12",
                    "serverName": "比翼飞"
                },
                {
                    "serverId": "lcmj-dzsf",
                    "minPrice": "352.10",
                    "createDate": "2023-11-12",
                    "serverName": "斗战胜佛"
                },
            ]
        }
        return store.mhbConf;
    }
}
store.baoshiimgs = {}
for (let e of store.rangeBaoshiCode) {
    if (e.stype) {
        store.baoshiimgs[e.value] = 'bs' + e.stype
    }
}

export default store