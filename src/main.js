import {createSSRApp} from "vue"

import App from "./App.vue";
import './uni.scss'
import {DateTime} from "luxon"
export function createApp() {
    console.log(DateTime.local().setZone('Asia/Shanghai').toISO())
	const app = createSSRApp(App);
    app.config.globalProperties.$today = DateTime.local().setZone('Asia/Shanghai').toISO().substring(0,10);
	return {
		app,
	};
}
