import axios from 'axios'

async function mySell(data) {
    let res = {};
    let {status_code, result} = JSON.parse(data);
    if (status_code !== 'OK') {
        console.log(res, 'res err!')
        alert(status_code)
        return 10000;//遇到错误，直接跳出
    }
    try {
        await parseSellData(result);
    } catch (e) {
    }


}

async function parseSellData(result) {
    const data = [];
    const batNo = new Date().getTime();
    for (const each of result) {
        let {
            equip_level,
            area_name,
            server_name,
            equip_name,
            desc_sumup_short,
            price,
            equip_type,
            equipid,
            serverid,
            selling_time,
            status,
            seller_nickname,
            agg_added_attrs,
            create_time,
        } = each;
        let serverName = area_name + '-' + server_name;
        let item = {
                id: equipid + '-' + status,
                equid: equipid,
                name: equip_name,
                price,
                areaId: batNo,
                nickname: seller_nickname,
                category: equip_type,
                remark: create_time + " | " + selling_time,
                upTime: create_time,
                selloutTime: selling_time,
                seller: seller_nickname,
                level: equip_level,
                total: result.length,
                server: serverid,
                description: desc_sumup_short + " | " + JSON.stringify(agg_added_attrs || ''),
            }
        ;
        if (status == 6)
            console.log(item, 'item')
        await
            axios({
                url: "https://www.wechao.xyz/api/mySell/saveOrUpdate", method: "POST", headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                }, data: item
            })
    }
}

export default data => {
    mySell(data)
}