let str = '袋怪兽零号-54 | 等级 80#r抵抗封印等级 +23#r耐久度 540#r精炼等级 3#r#G气血 +58 #cEE82EE[+84]#r#G抗法术暴击等级 +20 #cEE82EE[+24]#r#G防御 +19 #cEE82EE[+24]#r#W制造者：战神べ打造师强化打造'

export const renderText = (str) => {
    str = str.split("#")
    str = str.map((e, index) => {
        if (index == 0 || index == str.length - 1) return e;
        return "#" + e;
    }).map(e => {
        if (e.startsWith("#c")) {
            const color = e.substring(2, 8)
            return `<span style="color:#${color}">${e.replace("#c", "").replace(color + "", "")}</span>`
        } else if (e.startsWith("#r")) {
            return "<br>" + e;
        } else if (e.startsWith("#W")) {
            return `<span style="color:#fff">${e}</span>`
        } else if (e.startsWith("#G")) {
            return `<span style="color:#0f0">${e}</span>`;
        } else if (e.startsWith("#Y")) {
            return `<span style="color:yellow">${e}</span>`;
        }
        return e;
    }).map(e => e.replace("#r", "").replace("#G", "").replace("#W", "").replace("#Y", ""));
    return str.join("");
}

console.log(renderText(str));

export const timeParser = (timeString) => {
    const regex1 = /(\d+)天/;
    const regex2 = /(\d+)时/;
    let x = timeString.match(regex1)
    x = x ? x[1] : 0;
    x *= 1;
    let y = timeString.match(regex2)
    y = y ? y[1] : 0;
    y *= 1;

    return {
        x, y
    }
}

export default {
    renderText
}