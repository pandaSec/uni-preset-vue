import zb0 from "./imgs/zb-0hmg.jpg"
import zb1 from "./imgs/zb-1qchh.jpg"
import zb3 from "./imgs/zb-3swdp.jpg"
import zb4 from "./imgs/zb-4qbty.jpg"
import zb5 from "./imgs/zb-5wlzyl.jpg"
import zb6 from "./imgs/zb-6wljy.jpg"
import zb7 from "./imgs/zb-7jfzl.jpg"
import zb8 from "./imgs/zb-8bgdd.jpg"
import zb9 from "./imgs/zb-9swz.jpg"
import bs4002 from "./images/4002.gif"
import bs4003 from "./images/4003.gif"
import bs4004 from "./images/4004.gif"
import bs4010 from "./images/4010.gif"
import bs4011 from "./images/4011.gif"
import bs4012 from "./images/4012.gif"
import bs4244 from "./images/4244.gif"
import bs4249 from "./images/4249.gif"

export default {
    zb0,
    zb1,
    zb3,
    zb4,
    zb5,
    zb6,
    zb7,
    zb8,
    zb9,
    bs4002,
    bs4003,
    bs4004,
    bs4010,
    bs4011,
    bs4012,
    bs4249,
    bs4244
}