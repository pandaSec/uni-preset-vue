import Dexie from 'dexie'

export const db = new Dexie("mhxy-db");
db.version(1).stores({
    friends: '++id,name,age',
})

