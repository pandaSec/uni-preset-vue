import axios from "axios";

export function groupByMax(arr, group, valProp) {
    const groups = arr.reduce((result, item) => {
        const key = item[group];
        const value = item[valProp];
        if (!result[key] || item[value] > result[key][value]) {
            result[key] = item;
        }
        return result;
    }, {});

    return Object.values(groups);
}

export function groupByMin(arr, group, valProp) {
    const groupedData = arr.reduce((result, item) => {
        const key = item[group];
        const value = item[valProp];

        // 如果结果对象中不存在当前键，则创建一个新的键值对
        if (!result[key]) {
            result[key] = value;
        } else {
            // 如果存在当前键，比较当前值与已存在的值，保留较小的值
            result[key] = Math.min(result[key], value);
        }

        return result;
    }, {});
    return groupedData;
}

const data = [
    {group: "A", value: 5},
    {group: "A", value: 8},
    {group: "B", value: 3},
    {group: "B", value: 10},
    {group: "C", value: 6},
    {group: "C", value: 2}
];

const result = groupByMax(data, "group");
console.log(result);

export function groupBy(list, keyGetter, order) {
    const map = new Map();
    list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
            map.set(key, [item]);
        } else {
            collection.push(item);
        }
    });
    return map;
}


export async function getOptionCore({url, params, legendField, valueField, xField, valueHandler, dataHandler}) {
    console.log(url, 'url')
    console.log(params, 'url')
    let jsonData = await axios.get(url, {
        params
    })
    jsonData = jsonData.data;
    if (dataHandler)
        jsonData = dataHandler(jsonData);
    let list = jsonData.filter(e => !!e[legendField]);
    let map = groupBy(list, e => e[legendField]);
    let seriesNames = [...map.keys()]
    let xArrCol = groupBy(list, e => e[xField])
    let xArr = [...xArrCol.keys()].sort()
    let series = []
    seriesNames.map(name => {
        let data = xArr.map(xi => {
            return list.filter(e => e[xField] == xi && e[legendField] == name).map(e => e[valueField])[0]
        });
        if (valueHandler) {
            data = data.map(value => {
                return valueHandler(value)
            });
        }
        series.push({
            name,
            type: 'line',
            // stack: 'Total',
            data
        })
    })
    return {
        seriesNames,
        xArr,
        series
    }
}

// 示例使用
const students = [
    {id: 1, name: 'John', age: 20},
    {id: 2, name: 'Jane', age: 19},
    {id: 3, name: 'Peter', age: 20},
    {id: 4, name: 'Mary', age: 19},
    {id: 5, name: 'David', age: 20}
];

const groupedStudents = groupBy(students, (student) => student.age);
console.log(groupedStudents);

export function nameGroup(e) {
    if(!e.groupName)
    e.groupName = e.avatarText.substr(0, 5);
    // if (e.groupName.match(/.*00[14]/)) {
    //     e.groupName = e.groupName.substring(0, 2) + '001-004';
    // } else if (e.groupName.match(/.*00[25]/)) {
    //     e.groupName = e.groupName.substring(0, 2) + '002-005';
    // } else if (e.groupName.match(/.*00[36]/)) {
    //     e.groupName = e.groupName.substring(0, 2) + '003-006';
    // } else if (e.groupName.match(/.*g0[13]/)) {
    //     e.groupName = e.groupName.substring(0, 2) + 'g01-g03';
    // } else if (e.groupName.match(/.*[dk]k/)) {
    //     e.groupName = e.groupName.substring(0, 2) + 'dk-kk';
    // } else if (e.groupName.match(/.*(204)|(304)/)) {
    //     e.groupName = e.groupName.substring(0, 2) + '204-304';
    // } else if (e.groupName.match(/.*(201)|(301)/)) {
    //     e.groupName = e.groupName.substring(0, 2) + '201-301';
    // } else if (e.groupName.match(/.*(102)|(103)/)) {
    //     e.groupName = e.groupName.substring(0, 2) + '102-103';
    // }
    return e;
}