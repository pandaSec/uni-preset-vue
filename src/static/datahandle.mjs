let arr = [
    {
        "index": 19,
        "port": 12,
        "deviceName": "九天001"
    },
    {
        "index": 19,
        "port": 15,
        "deviceName": "九天001"
    },
    {
        "index": 19,
        "port": 22,
        "deviceName": "九天004"
    },
]

//group by order by max min avg where sum


export function groupBy(list, field) {
    const map = {}
    list.forEach((item) => {
        const key = item[field]
        const collection = map[key];
        if (!collection) {
            map[key] = [item];
        } else {
            collection.push(item);
        }
    });
    console.log(map, 'map')
    return function (action, prop) {
        return group(map, prop, action);
    };
}

function maxCore(arr, field) {
    let maxItem = undefined;
    arr.map(item => {
        if (!maxItem) {
            maxItem = item;
        } else {
            if (maxItem[field] < item[field]) {
                maxItem = item;
            }
        }
        return item;
    }, {});
    return maxItem[field];
}

function minCore(arr, field) {
    let minItem = undefined;
    arr.map(item => {
        if (!minItem) {
            minItem = item;
        } else {
            if (minItem[field] > item[field]) {
                minItem = item;
            }
        }
        return item;
    }, {});
    return minItem;
}

function sumCore(arr, field) {
    let sum = 0;
    arr.map(item => {
        sum += item[field]
        return item;
    }, {});
    return sum;
}

function countCore(arr, field) {
    return arr.length;
}

function avgCore(arr, field) {
    let sum = 0;
    arr.map(item => {
        sum += item[field]
        return item;
    }, {});
    return sum * 1.0 / arr.length;
}


function group(obj, field, action) {
    Object.keys(obj).map(key => {
        const val = obj[key];
        obj[key] = action(val, field);
    })
    return Object.keys(obj).map(e => {
        return {
            [field]: e,
            [action.name + "('" + field +
            "')"]: obj[e]
        }
    });
}


const result = groupBy(arr, "deviceName")(countCore, 'port')
// const result = group(groupBy(arr, "deviceName"), 'port',maxCore);
console.log(JSON.stringify(result))
console.log(result)